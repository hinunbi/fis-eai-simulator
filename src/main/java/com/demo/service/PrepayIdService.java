package com.demo.service;

import com.demo.model.ObjectFactory;
import com.demo.model.PrepayIdRequest;
import com.demo.model.PrepayIdResponse;
import org.springframework.stereotype.Component;

@Component
public class PrepayIdService {

  ObjectFactory objectFactory = new ObjectFactory();

  public PrepayIdResponse createPrepayId(PrepayIdRequest prepayIdRequest) {
    PrepayIdResponse  prepayIdResponse = objectFactory.createPrepayIdResponse();

    PrepayIdResponse.Xml xml = objectFactory.createPrepayIdResponseXml();

    xml.setReturnCode("000");
    xml.setReturnMsg("PrepayIdRequest is created successfully.");
    prepayIdResponse.setXml(xml);

    return prepayIdResponse;

  }
}
